package Exam;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class S2 extends JFrame {

    JTextField text;
    JButton btn;

    S2() {
        setTitle("Reverse String");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400, 400);
        setVisible(true);
    }

    void init() {
        setLayout(null);
        text = new JTextField();
        text.setBounds(20, 20, 200, 300);

        btn = new JButton("Press here");
        btn.setBounds(220, 160, 100, 50);
        btn.addActionListener(new Buton());

        add(text);
        add(btn);
    }

    class Buton implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            String str = text.getText();
            String rev = "";
            for (int i = str.length() - 1; i >= 0; i--) {
                rev = rev + str.charAt(i);
            }
            System.out.println(rev);
        }
    }

    public static void main(String[] args) {
        new S2();
    }
}



