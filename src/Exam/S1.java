package Exam;

public class S1 {
    public static void main(String[] args) {

    }
}

interface I {
}

class B implements I {
    private String param;
    private C c;

    public B() {
        this.c = new C();
    }

    public void metZ(Z z) {
    }
}

class C {
}

class E {
    private B b;

    public E(B b) {
        this.b = b;
    }
}

class Y {
    private B b;

    public void f() {
    }
}

class Z {
    public void g() {
    }
}
